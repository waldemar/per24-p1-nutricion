from alimento import Alimento
from categoriaAlimentos import CategoriaAlimentos
from nutriente import Nutriente
from typing import List, Dict
from persona import Persona
from persona import NivelActividad
class Dieta:
    """
    Representa una dieta diaria que contiene una serie de alimentos.
    """

    def __init__(self, nombre: str) -> None:
        """
        Constructor de la clase Dieta.

        :param str nombre: Nombre de la dieta.
        :param list[list[str]] semana: Lista de listas que representa los alimentos para cada día de la semana.
            Cada sublista contiene los nombres de los alimentos para un día específico.
        """
        self.nombre: str = nombre
        self.semana: List[List[Alimento]] = [[] for _ in range(7)]
        self.contenido_nutricional: Dict[str, float] = {}
    def agregar_menu(self, dia: int, alimentos: List[Alimento]) -> None:
        """
        Agrega un menú para un día específico de la semana.

        :param int dia: Número del día de la semana (0 para lunes, 1 para martes, etc.).
        :param list[str] alimentos: Lista de nombres de alimentos para el menú de ese día.
        """
        for alimento in alimentos:
            if not isinstance(alimento, Alimento):
                raise ValueError("Los elementos de la lista 'alimentos' deben ser instancias de la clase Alimento.")
        if 0 <= dia < 7:  # Verifica que el día esté dentro del rango válido (de 0 a 6)
                    self.semana[dia].extend(alimentos)
                    self.calcular_contenido_nutricional() 
        else:
              raise ValueError("El día debe estar en el rango de 0 a 6.")

    def calcular_contenido_nutricional_dia(self, dia: int) -> Dict[str, float]:
        """
        Calcula el contenido nutricional de un día específico de la semana.

        :param int dia: Número del día de la semana (0 para lunes, 1 para martes, etc.).
        :return: Diccionario con los totales de calorías, proteínas, grasas y carbohidratos del día especificado.
        :rtype: dict[str, float]
        """
        total_calorias: float = 0
        total_proteinas: float = 0
        total_grasas: float = 0
        total_carbohidratos: float = 0

        alimentos_dia = self.obtener_alimentos_dia(dia)

        for alimento in alimentos_dia:
            total_calorias += alimento.nutrientes.calcular_calorias() # type: ignore
            total_proteinas += alimento.nutrientes.proteinas # type: ignore
            total_carbohidratos += alimento.nutrientes.carbohidratos # type: ignore
            total_grasas += alimento.nutrientes.grasas # type: ignore

        return {
            'calorias': total_calorias,
            'proteinas': total_proteinas,
            'carbohidratos': total_carbohidratos,
            'grasas': total_grasas
        }
    def calcular_contenido_nutricional(self) -> None:
        """
        Calcula el contenido nutricional de la dieta completa.

        :return: Diccionario con los totales de calorías, proteínas, grasas y carbohidratos de la dieta.
        :rtype: dict[str, float]
        """
        total_calorias: float = 0
        total_proteinas: float = 0
        total_grasas: float = 0
        total_carbohidratos: float = 0

        for alimentos_dia in self.semana:
            for alimento in alimentos_dia:
                total_calorias += alimento.nutrientes.calcular_calorias() # type: ignore
                total_proteinas += alimento.nutrientes.proteinas # type: ignore
                total_carbohidratos += alimento.nutrientes.carbohidratos # type: ignore
                total_grasas += alimento.nutrientes.grasas # type: ignore

        self.contenido_nutricional = {
            'calorias': total_calorias,
            'proteinas': total_proteinas,
            'carbohidratos': total_carbohidratos,
            'grasas': total_grasas
        }
        
    def mostrar_contenido_nutricional(self) -> None:
        if self.contenido_nutricional is None:
            print("No se ha calculado el contenido nutricional.")
        else:
            print("Contenido Nutricional de la Dieta:")
            for nutriente, valor in self.contenido_nutricional.items():
                print(f"{nutriente.capitalize()}: {valor}")
                
    def obtener_alimentos_dia(self, dia: int) -> List[Alimento]:
        """
        Obtiene los alimentos de un día específico de la semana.

        :param int dia: Número del día de la semana (0 para lunes, 1 para martes, etc.).
        :return: Lista de nombres de alimentos del día especificado.
        :rtype: list[str]
        """
        if 0 <= dia < len(self.semana):
            return self.semana[dia]
        else:
            return []

    def evaluar_dieta(self, Persona1: Persona) -> str:
        """
        Evalúa la dieta en función de las calorías y el metabolismo basal.

        :param Persona Persona1: Objeto de la clase Persona.
        :return: Resultado de la evaluación de la dieta.
        :rtype: str
        """
        requerimiento_calorico = Persona1.calcular_requerimiento_calorico()


        if Persona1.sexo.lower() == 'hombre':
            metabolismo_basal = 66.473 + (13.751 * Persona1.peso) + (5.0033 * Persona1.altura) - (6.7550 * Persona1.edad)
        elif Persona1.sexo.lower() == 'mujer':
            metabolismo_basal = 655.1 + (9.463 * Persona1.peso) + (1.8 * Persona1.altura) - (4.6756 * Persona1.edad)
        else:
            raise ValueError("El sexo debe ser 'hombre' o 'mujer'.")

        calorias_dieta = self.contenido_nutricional.get('calorias', 0)
        producto_metabolismo = requerimiento_calorico * metabolismo_basal
        
        if (producto_metabolismo - 200) <= calorias_dieta <= (producto_metabolismo + 200):
            return "La dieta es adecuada"
        elif calorias_dieta > producto_metabolismo:
            return "La dieta es mala"
        else:
            return "La dieta es buena"
        
    def eliminar_alimento(self, dia: int, indice: int) -> None:
        """
        Elimina un alimento específico de un día de la semana.

        :param int dia: Número del día de la semana (0 para lunes, 1 para martes, etc.).
        :param int indice: Índice del alimento a eliminar en la lista de alimentos de ese día.
        """
        if 0 <= dia < 7:
            if 0 <= indice < len(self.semana[dia]):
                del self.semana[dia][indice]
            else:
                raise IndexError("Índice fuera de rango.")
        else:
            raise ValueError("El día debe estar en el rango de 0 a 6.")
      
 
        

nutriente_manzana = Nutriente(proteinas=0.3, carbohidratos=25, grasas=0.4, fibra=4.4, colesterol=0)
nutriente_pollo = Nutriente(proteinas=31, carbohidratos=20000.2, grasas=4.8, fibra=0, colesterol=85)
nutriente_cebolla = Nutriente(proteinas=321, carbohidratos=2.2, grasas=2.8, fibra=1, colesterol=85)
nutriente_pera = Nutriente(proteinas=3221, carbohidratos=22.2, grasas=2.8, fibra=1, colesterol=2)

manzana = Alimento(nombre="Manzana", nutrientes=nutriente_manzana, categoria=CategoriaAlimentos.CARNES)
pollo = Alimento(nombre="Pollo", nutrientes=nutriente_pollo, categoria=CategoriaAlimentos.CARNES)
cebolla = Alimento(nombre="Cebolla", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.VERDURAS)
pera = Alimento(nombre="Pera", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.FRUTAS)

dieta_semana = Dieta(nombre="Dieta Semanal")
dieta_semana.agregar_menu(0, [manzana, pollo])
dieta_semana.agregar_menu(1, [pera, cebolla])

contenido_nutricional = dieta_semana.calcular_contenido_nutricional()
[print(','.join((alimento.nombre) for alimento in dieta_semana.obtener_alimentos_dia(2)))]

dieta_semana.mostrar_contenido_nutricional()
persona1 = Persona(peso=100, altura=1.80, edad = 20, sexo='hombre', actividad=NivelActividad.HIPERACTIVO )

print(dieta_semana.evaluar_dieta(persona1))

print(dieta_semana.evaluar_dieta(persona1))