# Práctica Nutrición
![UML DEL PROYECTO NUTRICION](https://www.planttext.com/api/plantuml/png/ZLRTRvim47_dh_1ZgfA7VRqWhIfAc1QjJQeaZLHLbUN0DRC4EBDDDMdh_xx3ne09JST3PExEl_lwtP5hfPdKLP47GPepfSYoqf93gO7y2GW-qwCK6dZ9L4IUSy6qbQPCxiLFdaccXQyvIAO6nizyBvb_NUIWD4YHEx6HNrBAIwufdGozXcJCOqWyRo7fUy8hGoyNralAyhJAcQHu498pDRbmORm60PHLGMPCmw7MnJali1n2DVNujhxVnXjpdCNhPMAFksGzlrytyfjujarMczPyjQ6RP8Q2LrTa1-fgN8fYBo4YIij6uBg0gRKTSPLl8ej6WXoMiGSSab7Gkh1d09kA7K1JYrMNoeLfnIrghVA3DGOjVgqV2TaOgiv1B-OQK9JGC-cSZMPpSg5pVZxDvfvh_vA_G1wdch_mZ6LDcpR9F5bku_LYHRwI3xRLoIwvHM52iUUBNQtuQ1ItgtcoZkVnGFd9AkyVEj5d8_g-k4Dx9_lIKUKEf18bSqoXHr326oNASbqDnei2HXqHNhft1Rz5ZrwKkTIYGQedSzYv2uddAIIjav2q-6Zq5lHm-2Jyga3oecwSQ4On5TuesXBCEMZsvgWeA5Y9GTrmfH_DZ-lgqvDRDiYCacV2qYVbM4_S27EUwaSJil7tT6Pscj3PGS818qVqQfBLJ3B9CyUY9eZMlyT6G2akD4cTUG-1bnbFmRnuTxf5DPR2nCT_ONc5fbbTBUoReKta-DEVMh57BEW2KNY7jT5S3BFudr2629YEfTvb8T3lkndulVu1pMfyloMWRCA8BoMasYoPlH3v0787k8eOZdmxIWrxgcCENlSG-nmLaEkZLnoxHO6VhdslpPILlB2fchWy9dLOa-vuklfQxhnlZsctNvVfjFSvnxM7BS0pa2lFPEHJPsXJaoG5mj3SBQ4QqD-C4Tasa7PAqQB5zZnsZPrE_rxPfWGdhUdfWcieC_F7f7x-0G00)`_UMl del Proyecto._`

El objetivo principal de la práctica es diseñar un esqueleto inicial de clases en Python que sirva como base para la manipulación de información nutricional. Este proyecto sienta las bases para futuras prácticas donde probablemente se lleve a un servidor web y se le incluya la parte del front.

## Estructura 🚀

El proyecto consta de 14 ficheros Python (pruebas unitarias, recursos, readme..) que implementan las funcionalidades necesarias, aunque lo más destacados son los siguientes:

- `nutriente.py:` Representa los nutrientes y proporciona métodos para su gestión.
- `categoriaAlimento`.py: Define las diferentes categorías de alimentos.
- `alimento.py:` Representa un alimento junto con sus atributos y métodos asociados.
- `dieta.py:` Modelo para una dieta diaria con métodos para gestionarla.
- `dietaVegetariana.py:` Implementa una dieta vegetariana con sus características específicas.
- `dietaKeto.py:` Define una dieta cetogénica y sus requisitos.
- `unittests.py:` Contiene la batería de pruebas unitarias para garantizar el correcto funcionamiento del código.
- `persona.py:` Define la clase Persona que representa a un individuo con atributos relacionados con su composición corporal.

## Cuestiones ❓

**-¿Ha usado herencia? ¿Dónde?** Justifica las razones de haberla usado o no.

**Sí,** se ha utilizado herencia en el proyecto. Las clases **DietaKeto y DietaVegetariana** heredan de la clase base **Dieta**. 

Se utiliza la herencia para aprovechar la estructura y funcionalidades comunes de una dieta. Esto quiere decir que nos interesa heredar sus métodos y sus atributos, para reescribirlos, usarlos sin tener que volver a definir el mismo método del padre o incluso ampliarlos creando nuevos métodos en la clase hija si así se desea.

 En el caso de este proyecto en concreto en clase **DietaKeto**, se han aprovechado el método

 ` calcular_contenido_nutricional()`, y el atributo del padre `contenido_nutricional:dict`, evitando duplicar código y facilitando la extensión del sistema en el futuro.

 Con calcular_contenido_nutricional() el padre recibe en su atributo contenido_nutricional un diccionario que calcula a través de los alimentos que tiene.

 Otra particularidad en esta clase es el uso de super(), se usa para sobreescribir el atributo nombre del padre;


        
    from dieta import Dieta
#importante importar para usar la clase.

     class DietaKeto(Dieta):
    #Para heredar en py se mete la clase padre en el argumento de la clase.
        def __init__(self, nombre: str) -> None:
        #se reescribe el atributo nombre 
        super().__init__(nombre) 
        #para llamar al constructor del padre hay que invocarlo mediente super()

Podría haber usado otra alternativa para reescribir el atributo nombre del a clase padre:

       
                                                
        
     class DietaKeto(Dieta):
      def __init__(self, nombre:str)

        # Alternativa 1
        # self.nombre = nombre

 

        # Alternativa 2
        super().__init__(nombre)
        
En el caso de la Clase **DietaVegetariana**
Hereda de la clase Dieta sus métodos y atributos;
El atributo `nombre: str` desde la clase hijo se inicializa en la del padre y se crean dos nuevos atributos: `semana y sumplementos  `
    
     class dietaVegetariana(Dieta):
     #se hereda de la clase dieta
    def __init__(self, nombre: str, suplementos: List[str]) -> None:
    #se inicializan en la clase hijo suplementos y nombre.
        super().__init__(nombre) 
        # de los atributos inicializados se le pasa al padre el atributo nombre  
        self.semana: List[List[Alimento]] = [[] for _ in range(7)]
        #este ha sido un error IMPORTANTE DE MECIONAR
        #YA QUE LO HE CREADO Y NO HA AFECTADO AL FUNCIONAMIENTO DEL CÓDIGO PORQUE SE INICIALIZA VACÍO
        #PERO QUERÍA ACCEDER AL DEL PADRE NO AL DEL HIJO, ESTA LINEA HAY QUE OBVIARLA EN LA CLASE HIJO
        self.suplementos: List[str] = suplementos

- [ ] Su justificación principal es poder crear objetos DietaVegetariana y DietaKeto, aprovechando los métodos de dieta y creando unos nuevos propios de funcionalidad como `es_dieta_correcta()` en la clase DietaKeto , así no hay que volver a crear todos los métodos y atributos de dieta como el mencionado anteriormente  `calcular_contenido_nutricional()`, esto nos permite que solo con poner el nombre entre paréntesis podemos acceder a ellos (reutilizando código y simplificando el problema)y en DietaVegetariana permite acceder al atributo de la clase Dieta     
    `self.semana: List[List[Alimento]]`
sin tener que volver a reescribirlo, en mi caso lo escribí por error pero es una errata y lo dejo para ver como si está vacío PYTHON accede al del padre aunque esté inicializado vacío en la clase del hijo, en el caso de que lo hubiera completado con un alimento y un nutriente hubiera accedido al del hijo y no al del padre se hubiera sobreescrito esa variable.

 

**-¿Has contemplado en tu diseño aspectos de visibilidad de atributos?** Desarrolla este concepto e indica algún punto de tu código en el que se observe su uso (de haberlo usado).


sí, he contemplado aspectos de visibilidad, en python estos se suelen realizar a través de **convenciones y el uso de decoradores** (en setters y getters). 
Para el desarrollo del proyecto se ha usado la convención de barra baja (_) antes del identificador para "privatizar" los atributos conocida como _"name mangling"_.

clase Persona:

     self._peso = peso
     self._altura = altura
     self._edad = edad
     self._sexo = sexo.lower()

clase Alimento:

     self._nombre = nombre
     self._nutrientes = nutrientes
     self._categoria = categoria


En cuanto a decodadores, he usado el decorador @property como getter y @nombre.setter como decodador setter, en la clase Alimento y persona en todos sus métodos de propiedad he aplicado estos decodadores.



    @property
    def nombre(self) -> str:
        """Getter para el nombre del alimento."""
        return self._nombre

    @nombre.setter
    def nombre(self, value: str) -> None:
        """Setter para el nombre del alimento."""
        self._nombre = value

    @property
    def nutrientes(self) -> object:
        """Getter para los nutrientes del alimento."""
        return self._nutrientes

    @nutrientes.setter
    def nutrientes(self, value: object) -> None:
        """Setter para los nutrientes del alimento."""
        self._nutrientes = value

    @property
    def categoria(self) -> CategoriaAlimentos:
        """Getter para la categoría del alimento."""
        return self._categoria

    @categoria.setter
    def categoria(self, value: CategoriaAlimentos) -> None:
        """Setter para la categoría del alimento."""
        self._categoria = value


estos decodadores, metodos modificadores y accesores, o de eliminación, lo que permiten es encapsular mejor los atributos y acceder a ellos de una manera más controlado evitando accesos indeseados, también se pueden usar para crear una interfaz de lógica entre el atributo y el acceso conviertiendo así en propiedad ese método, en python como sabemos se puede seguir accediendo con la nomeclatura . al atributo , por lo que con estos métodos se evita acceder directamente al atributo.



**-¿Alguna clase tiene referencias a otra clase?** Es decir, ¿alguna clase tiene un atributo que sea una referencia a una instancia de otra clase?

Sí, en el UML de este documento es donde mejor se puede apreciar estas referencias, Dieta tiene referencias de alimento en su atributo self.semana, referencia a una sublista de objetos de la clase Alimento.

`self.semana: List[List[Alimento]] = [[] for _ in range(7)]`


en el método evaluar_dieta() también se referencia a la clase Persona, ya que su argumento necesita introducir un objeto persona.

`def evaluar_dieta(self, Persona1: Persona) -> str:`

En la clase Persona hay una referencia a otra clase que sería NivelActividad en el atributo actividad.

`def __init__(self, peso: float, altura: float, edad: int, sexo: str, actividad: NivelActividad) -> None:
`

`self._actividad = actividad
`

En la clase **Alimento** a su vez hay referencia a la clase Nutriente y categoríaAlimento, ya que se necesita pasar esas instancias de esas clases (objetos) cuando creas un objeto Alimento y esos objetos inicializan en sus atributos esas intancias:

    def __init__(self, nombre: str, nutrientes: Nutriente, categoria: CategoriaAlimentos) -> None:
          """
            Inicializa un nuevo objeto Alimento.

            :param str nombre: Nombre del alimento.
            :param object nutrientes: Objeto que representa los nutrientes del alimento.
            :param CategoriaAlimento categoria: Categoría del alimento (enum).
            """
            self._nombre = nombre
            self._nutrientes = nutrientes
            self._categoria = categoria

_extra: (aunque en la segunda parte de la pregunta no se pide, pero es interesante mencionar como referencia)_
Además de las referencias identificadas previamente, también se observa el uso de herencia en el diseño de clases. Las clases DietaKeto y dietaVegetariana heredan de la clase base Dieta, lo que establece una relación de subclase-superclase entre ellas. Esta relación implica que las subclases hacen referencia a la superclase, ya que tienen acceso a todos los atributos y métodos de la clase base. La herencia se refleja en la forma en que las subclases extienden y especializan el comportamiento de la clase base, lo que contribuye a la reutilización de código y la organización jerárquica del sistema

-**¿Es lo mismo funciones que métodos?** Justifica tú respuesta. ¿Has usado métodos y/o funciones en tu código?. ¿Dónde? Responde diciendo exactamente en qué fichero y en qué línea lo has usado.

No, no son lo mismo. Los métodos son funciones que pertenecen a una clase y están asociados a objetos específicos de esa clase. Se distinguen de las funciones en que los métodos tienen un parámetro self que hace referencia al objeto en sí mismo, mientras que las funciones no tienen esta referencia implícita.

He usado tanto métodos como funciones en mi código.Métodos los he usado en;

 archivo: _dieta.py_
En la clase Dieta linea 23:
`def agregar_menu(self, dia: int, alimentos: List[Alimento]) -> None:`


 linea 66:
`def calcular_contenido_nutricional_dia(self, dia: int) -> Dict[str, float]:
`

 linea 92: 
  `def mostrar_contenido_nutricional(self) -> None:`

 linea 100:
 `def obtener_alimentos_dia(self, dia: int) -> List[Alimento]:`
 
  linea 113:
  `def evaluar_dieta(self, Persona1: Persona) -> str:`

  linea 141:
  `def eliminar_alimento(self, dia: int, indice: int) -> None:`

Ejemplo de una función en el proyecto, se ha creado una clase main para crear una función que convierta el valor booleano de comprobar_alimento_animal() a un print por pantalla.

 archivo main.py
linea 37-41:

`def imprimirporpantalla(dieta: dietaVegetariana) -> None:
    if  not dieta.comprobar_alimento_animal():
        print("La dieta es vegetariana.")
    else:
        print("La dieta contiene alimentos de origen animal.")
`
como se aprecia es una función ya que no pertenece a ninguna clase, a parte de estar fuera no tiene un argumento o parametro que lo lige a una clase, por defecto se suele llamar self.

también mencionar que hay distintos tipos de métodos en python,


- de instancia que son la mayoría que se han enunciado, 


- de clase que tendríamos uno en el arhivo nutriente.py en la linea 
145-146
`@classmethod
    def obtener_total_nutrientes(cls):
`
y estáticos que no habría ninguno en este proyecto y que no suelen ligarse ni a la clase ni a la instancia, sería como una función que se encierra en la clase para que esté mejor clasificada, que usa decorador, pero que no se liga a la clase o la instancia.

-**¿Es lo mismo referencias que instancias?** Justifica muy bien tu respuesta. Indica algún punto de tu código en el que se creen instancias. Indica alguno en el que se copien o modifiquen referencias.

No, una referencia es cuando se asigna un objeto a un espacio en memoria y una instancia es cuando se crea ese objeto.

Para entender porque no es lo mismo es interesante extender la respuesta de que es una y otra,

una referencia se crea por ejemplo con la variable `a = objeto1()` , estaría creando una instancia del objeto1 y una referencia con la variable "a" que apuntaría al espacio en memoria donde está esa instancia objeto1.

pero puedo crear otra referencia que apunte a esa misma instancia objeto1  ` b = a `

este se debe a que en python cuando asignas un objeto a otra variable, asignas la referencia al objeto, entonces `b` apunta donde apunte `a` pero no guarda el valor.

para indicar puntos donde se creen instancias escojo el archivo main.py ya que es el recipiente donde he decidido crear diferente instancias de diferentes clases.

de la linea 12-15 he creado diferentes instancias de  la clase nutriente o también se puede decir que he creado objeto de esa clase.

    nutriente_manzana = Nutriente(proteinas=0.3, carbohidratos=25, grasas=0.4, fibra=4.4, colesterol=0)
    nutriente_pollo = Nutriente(proteinas=31, carbohidratos=20000.2, grasas=4.8, fibra=0, colesterol=85)
    nutriente_cebolla = Nutriente(proteinas=321, carbohidratos=2.2, grasas=2.8, fibra=1, colesterol=85)
    nutriente_pera = Nutriente(proteinas=3221, carbohidratos=22.2, grasas=2.8, fibra=1, colesterol=2)

de la linea 17-20 he creado instancias de la clase alimento una otra vez por linea

    manzana = Alimento(nombre="Manzana", nutrientes=nutriente_manzana, categoria=CategoriaAlimentos.FRUTAS)
    pollo = Alimento(nombre="Pollo", nutrientes=nutriente_pollo, categoria=CategoriaAlimentos.CARNES)
    cebolla = Alimento(nombre="Cebolla", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.VERDURAS)
    pera = Alimento(nombre="Pera", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.FRUTAS)

en la linea 22 he creado una instancia de la clase dieta:

`dieta_semana = Dieta(nombre="Dieta Semanal")
`
ahora viendo las lineas de código anterior como se ha mencionado es una instancia por linea de código , también coincide con el mismo número de creación de referencias 
en vez de mirar la parte derecha del "=" 
por ejemplo:

`=Alimento(nombre="Pera", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.FRUTAS)`

para la referencia habría que fijarse en el lado izquierdo que es la "variable" o referencia donde apunta a la instancia ejemplo de esa misma linea la referencia sería pera = ...  siendo una sola referencia asignada ahí.

Donde podemos encontrar ejemplos donde se han modificado o copiados referencias, es la clase dieta.py

en el constructor se crean referencias por ejemplo linea 21-22 referencia a una lista de 7 sublsitas vacias y a un diccionario clave valor vacio, 

    self.semana: List[List[Alimento]] = [[] for _ in range(7)]
    self.contenido_nutricional: Dict[str, float] = {}

después se ha modificado la referencia a través de los métodos por ejemplo de agregar_menu() de la linea 23, se modifican las referencias en la lista semana al agregar instancias de la clase Alimento.

    def agregar_menu(self, dia: int, alimentos: List[Alimento]) -> None:
                """
                Agrega un menú para un día específico de la semana.

                :param int dia: Número del día de la semana (0 para lunes, 1 para martes, etc.).
                :param list[str] alimentos: Lista de nombres de alimentos para el menú de ese día.
                """
                for alimento in alimentos:
                    if not isinstance(alimento, Alimento):
                        raise ValueError("Los elementos de la lista 'alimentos' deben ser instancias de la clase Alimento.")
                if 0 <= dia < 7:  # Verifica que el día esté dentro del rango válido (de 0 a 6)
                            self.semana[dia].extend(alimentos)
                            self.calcular_contenido_nutricional() 
                else:
                    raise ValueError("El día debe estar en el rango de 0 a 6.")

        

en la linea 141 con el método     
`def eliminar_alimento(self, dia: int, indice: int) -> None:`

de la clase Dieta, se modifica la referencia en la lista semana al eliminar un elemento de la lista.


## Construido con 🛠️

Con Python, destacando herencia, clases, enums, la biblioteca typing y la biblioteca unittest para pruebas unitarias
## Contribuyendo 🖇️
 A la espera de compañeros que se sumen.

## Versionado 📌

Versión 1.0

## Curiosidades / Recursos extras 📌

He descubierto con el desarrollo de la práctica que se puede inicializar atributos directamente usando solamente un método:

` setattr(self, nombre_atributo, valor)
`
y que se puede crear un constructor propio así:

`

    class MiClase:
    def __init__(self):
        pass

    def establecer_atributos(self, nombre_atributo, valor):
        setattr(self, nombre_atributo, valor)

    # Crear una instancia de la clase
    objeto = MiClase()

    # Establecer atributos utilizando el método establecer_atributos
    objeto.establecer_atributos('atributo1', 42)
    objeto.establecer_atributos('atributo2', 'Hola')

    # Acceder a los atributos establecidos
    print(objeto.atributo1)  # Salida: 42
    print(objeto.atributo2)  # Salida: Hola


`
Otro recurso que he aprendido 

es el método call, que sirve para llamar a un objeto como una función

` 

    class MiClase:
    def __init__(self):
        pass

    def __call__(self, *args, **kwargs):
            print("Llamando a MiClase con argumentos:", args, kwargs)

    # Crear una instancia de la clase
    objeto = MiClase()

    # Llamar a la instancia como una función
    objeto(1, 2, nombre="John")
    `

otro recurso que he aprendido es el argumento especial **kwargs
`

    class Nutriente:
    def __init__(self, **kwargs):
            self.proteinas = kwargs.get('proteinas', 0)
            self.carbohidratos = kwargs.get('carbohidratos', 0)
            self.grasas = kwargs.get('grasas', 0)
            self.fibra = kwargs.get('fibra', 0)
            self.colesterol = kwargs.get('colesterol', 0)

    # Crear una instancia de la clase Nutriente con los valores proporcionados
    nutriente_manzana = Nutriente(proteinas=0.3, carbohidratos=25, grasas=0.4, fibra=4.4, colesterol=0)

    # Ahora puedes acceder a los atributos de esta instancia
    print(nutriente_manzana.proteinas)
    print(nutriente_manzana.carbohidratos)
    print(nutriente_manzana.grasas)
    print(nutriente_manzana.fibra)
    print(nutriente_manzana.colesterol)
`

investigando decoradores tambien he encontrado que hay una funcion y un decodador para eliminar propiedades 
`
        ** delattr(self, nombre_atributo)**


    @atributo.deleter
        def atributo(self):
            del self._atributo
            print("El atributo ha sido eliminado")`


Y por último he aprendido la diferencia entre atributo y propiedad de una clase

atributo característica intrínseca
propiedad es una interfaz de acceso a ese atributo (algo que añade lógica, valida.. etc)

he aprendido que self es una convención en Python y que puede tener cualquier nombre válido, 


## Autores ✒️


* **Waldemar Stegierski** - *Estudiante* - [linkedin](https://www.linkedin.com/in/waldemar-stegierski-2a6589209/)
  

## Licencia 📄

Este proyecto está bajo la Licencia MIT. Consulte el archivo [LICENSE](https://gitlab.eif.urjc.es/waldemar/per24-p1-nutricion/-/blob/main/LICENSE) para obtener más detalles.


## Expresiones de Gratitud 🎁

*Agradecimientos a  📢
* Rubén, Álvaro y Agustin por resolver en cualquier momento las dudas relacionadas con la asignatura.

## Aspectos a mejorar en el diseño 📌

Podría haber mejorado el proyecto con una clase abstracta en Dieta con el método init abstracto para que dietaVegetariana y DietaKeto tuvieran que recoger obligatoriamente el atributo nombre: str


---
