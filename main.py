from alimento import Alimento
from categoriaAlimentos import CategoriaAlimentos
from nutriente import Nutriente
from typing import List, Dict
from persona import Persona
from persona import NivelActividad
from dieta import Dieta
from dietaVegetariana import dietaVegetariana
 
        

nutriente_manzana = Nutriente(proteinas=0.3, carbohidratos=25, grasas=0.4, fibra=4.4, colesterol=0)
nutriente_pollo = Nutriente(proteinas=31, carbohidratos=20000.2, grasas=4.8, fibra=0, colesterol=85)
nutriente_cebolla = Nutriente(proteinas=321, carbohidratos=2.2, grasas=2.8, fibra=1, colesterol=85)
nutriente_pera = Nutriente(proteinas=3221, carbohidratos=22.2, grasas=2.8, fibra=1, colesterol=2)

manzana = Alimento(nombre="Manzana", nutrientes=nutriente_manzana, categoria=CategoriaAlimentos.FRUTAS)
pollo = Alimento(nombre="Pollo", nutrientes=nutriente_pollo, categoria=CategoriaAlimentos.CARNES)
cebolla = Alimento(nombre="Cebolla", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.VERDURAS)
pera = Alimento(nombre="Pera", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.FRUTAS)

dieta_semana = Dieta(nombre="Dieta Semanal")
dieta_semana.agregar_menu(0, [manzana, pollo])
dieta_semana.agregar_menu(1, [pera, cebolla])

contenido_nutricional = dieta_semana.calcular_contenido_nutricional()
[print(','.join((alimento.nombre) for alimento in dieta_semana.obtener_alimentos_dia(2)))]

dieta_semana.mostrar_contenido_nutricional()
persona1 = Persona(peso=100, altura=1.80, edad = 20, sexo='hombre', actividad=NivelActividad.HIPERACTIVO )

print(dieta_semana.evaluar_dieta(persona1))

print(dieta_semana.evaluar_dieta(persona1))


def imprimirporpantalla(dieta: dietaVegetariana) -> None:
    if  not dieta.comprobar_alimento_animal():
        print("La dieta es vegetariana.")
    else:
        print("La dieta contiene alimentos de origen animal.")

suplementos = ["Vitamina B12", "Omega-3"]
dieta_semana = dietaVegetariana("Dieta Semanal", suplementos)
dieta_semana.agregar_menu(0, [manzana, cebolla])
dieta_semana.agregar_menu(1, [pera, cebolla])

imprimirporpantalla(dieta_semana)