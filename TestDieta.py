import unittest
from alimento import Alimento
from categoriaAlimentos import CategoriaAlimentos
from nutriente import Nutriente
from persona import Persona, NivelActividad
from dieta import Dieta

class TestDieta(unittest.TestCase):
    def setUp(self):
        self.nutriente_manzana = Nutriente(proteinas=0.3, carbohidratos=25, grasas=0.4, fibra=4.4, colesterol=0)
        self.nutriente_pollo = Nutriente(proteinas=31, carbohidratos=20000.2, grasas=4.8, fibra=0, colesterol=85)
        self.nutriente_cebolla = Nutriente(proteinas=321, carbohidratos=2.2, grasas=2.8, fibra=1, colesterol=85)
        self.nutriente_pera = Nutriente(proteinas=3221, carbohidratos=22.2, grasas=2.8, fibra=1, colesterol=2)

        self.manzana = Alimento(nombre="Manzana", nutrientes=self.nutriente_manzana, categoria=CategoriaAlimentos.FRUTAS)
        self.pollo = Alimento(nombre="Pollo", nutrientes=self.nutriente_pollo, categoria=CategoriaAlimentos.CARNES)
        self.cebolla = Alimento(nombre="Cebolla", nutrientes=self.nutriente_cebolla, categoria=CategoriaAlimentos.VERDURAS)
        self.pera = Alimento(nombre="Pera", nutrientes=self.nutriente_pera, categoria=CategoriaAlimentos.FRUTAS)

        self.dieta_semana = Dieta(nombre="Dieta Semanal")

    def test_agregar_menu(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.assertEqual(len(self.dieta_semana.semana[0]), 2)

    def test_eliminar_alimento(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.dieta_semana.eliminar_alimento(0, 1)
        self.assertEqual(len(self.dieta_semana.semana[0]), 1)

    def test_calcular_contenido_nutricional(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.dieta_semana.agregar_menu(1, [self.pera, self.cebolla])
        self.dieta_semana.calcular_contenido_nutricional()
        self.assertNotEqual(self.dieta_semana.contenido_nutricional, {})
        self.assertIn('calorias', self.dieta_semana.contenido_nutricional)
        self.assertIn('proteinas', self.dieta_semana.contenido_nutricional)
        self.assertIn('carbohidratos', self.dieta_semana.contenido_nutricional)
        self.assertIn('grasas', self.dieta_semana.contenido_nutricional)

    def test_evaluar_dieta(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.dieta_semana.agregar_menu(1, [self.pera, self.cebolla])
        persona1 = Persona(peso=100, altura=1.80, edad=20, sexo='hombre', actividad=NivelActividad.HIPERACTIVO)
        self.dieta_semana.calcular_contenido_nutricional()
        resultado = self.dieta_semana.evaluar_dieta(persona1)
        self.assertIn(resultado, ["La dieta es adecuada", "La dieta es mala", "La dieta es buena"])

    def test_obtener_alimentos_dia(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        alimentos_dia_0 = self.dieta_semana.obtener_alimentos_dia(0)
        self.assertEqual(alimentos_dia_0, [self.manzana, self.pollo])

        alimentos_dia_1 = self.dieta_semana.obtener_alimentos_dia(1)
        self.assertEqual(alimentos_dia_1, [])

    def test_calcular_contenido_nutricional_dia(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.dieta_semana.agregar_menu(1, [self.pera, self.cebolla])
        contenido_nutricional_dia_0 = self.dieta_semana.calcular_contenido_nutricional_dia(0)
        contenido_nutricional_dia_1 = self.dieta_semana.calcular_contenido_nutricional_dia(1)

        self.assertEqual(contenido_nutricional_dia_0['calorias'], self.manzana.nutrientes.calcular_calorias() + self.pollo.nutrientes.calcular_calorias()) # type: ignore
        self.assertEqual(contenido_nutricional_dia_1['calorias'], self.pera.nutrientes.calcular_calorias() + self.cebolla.nutrientes.calcular_calorias()) # type: ignore

    def test_mostrar_contenido_nutricional(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.dieta_semana.agregar_menu(1, [self.pera, self.cebolla])
        self.dieta_semana.calcular_contenido_nutricional()

        contenido_nutricional_esperado = {
            'calorias': self.manzana.nutrientes.calcular_calorias() + self.pollo.nutrientes.calcular_calorias() + self.pera.nutrientes.calcular_calorias() + self.cebolla.nutrientes.calcular_calorias(), # type: ignore
            'proteinas': self.manzana.nutrientes.proteinas + self.pollo.nutrientes.proteinas + self.pera.nutrientes.proteinas + self.cebolla.nutrientes.proteinas, # type: ignore
            'carbohidratos': self.manzana.nutrientes.carbohidratos + self.pollo.nutrientes.carbohidratos + self.pera.nutrientes.carbohidratos + self.cebolla.nutrientes.carbohidratos, # type: ignore
            'grasas': self.manzana.nutrientes.grasas + self.pollo.nutrientes.grasas + self.pera.nutrientes.grasas + self.cebolla.nutrientes.grasas # type: ignore
        }

        self.assertEqual(self.dieta_semana.contenido_nutricional, contenido_nutricional_esperado)

if __name__ == '__main__':
    unittest.main()
