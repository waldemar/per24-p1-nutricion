import unittest
from persona import Persona, NivelActividad

class TestPersona(unittest.TestCase):
    def setUp(self):
        self.persona = Persona(peso=70, altura=175, edad=30, sexo='hombre', actividad=NivelActividad.MODERADAMENTE_ACTIVO)

    def test_init(self):
        self.assertEqual(self.persona.peso, 70)
        self.assertEqual(self.persona.altura, 175)
        self.assertEqual(self.persona.edad, 30)
        self.assertEqual(self.persona.sexo, 'hombre')
        self.assertEqual(self.persona.actividad, NivelActividad.MODERADAMENTE_ACTIVO)

    def test_calcular_requerimiento_calorico_sedentario(self):
        persona_sedentaria = Persona(peso=70, altura=175, edad=30, sexo='hombre', actividad=NivelActividad.SEDENTARIO)
        self.assertAlmostEqual(persona_sedentaria.calcular_requerimiento_calorico(), 1.2, places=2)

    def test_calcular_requerimiento_calorico_levemente_activo(self):
        persona_levemente_activa = Persona(peso=70, altura=175, edad=30, sexo='hombre', actividad=NivelActividad.LEVEMENTE_ACTIVO)
        self.assertAlmostEqual(persona_levemente_activa.calcular_requerimiento_calorico(), 1.375, places=2)

    def test_calcular_requerimiento_calorico_moderadamente_activo(self):
        self.assertAlmostEqual(self.persona.calcular_requerimiento_calorico(), 1.55, places=2)

    def test_calcular_requerimiento_calorico_muy_activo(self):
        persona_muy_activa = Persona(peso=70, altura=175, edad=30, sexo='hombre', actividad=NivelActividad.MUY_ACTIVO)
        self.assertAlmostEqual(persona_muy_activa.calcular_requerimiento_calorico(), 1.725, places=2)

    def test_calcular_requerimiento_calorico_hiperactivo(self):
        persona_hiperactiva = Persona(peso=70, altura=175, edad=30, sexo='hombre', actividad=NivelActividad.HIPERACTIVO)
        self.assertAlmostEqual(persona_hiperactiva.calcular_requerimiento_calorico(), 1.9, places=3)

    def test_calcular_requerimiento_calorico_invalido(self):
        with self.assertRaises(ValueError):
            persona_invalida = Persona(peso=70, altura=175, edad=30, sexo='hombre', actividad=NivelActividad(9))
            #creo una instancia de NiveldeActividad para que pase la excepción, si no no la pasaría porque saltaría una exepción antes de no pasar una categoría correcta

if __name__ == "__main__":
    unittest.main()
