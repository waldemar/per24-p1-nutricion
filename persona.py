from enum import Enum

class NivelActividad(Enum):
    SEDENTARIO = 1
    LEVEMENTE_ACTIVO = 2
    MODERADAMENTE_ACTIVO = 3
    MUY_ACTIVO = 4
    HIPERACTIVO = 5

class Persona:
    """
    Representa una persona con atributos relacionados con su composición corporal.
    """

    def __init__(self, peso: float, altura: float, edad: int, sexo: str, actividad: NivelActividad) -> None:
        """
        Constructor de la clase Persona.

        :param float peso: Peso en kilogramos.
        :param float altura: Altura en centímetros.
        :param int edad: Edad en años.
        :param str sexo: Sexo de la persona ('hombre' o 'mujer').
        :param NivelActividad actividad: Nivel de actividad física.
        """
        
        self._peso = peso
        self._altura = altura
        self._edad = edad
        self._sexo = sexo.lower()
        if actividad not in NivelActividad:
             raise ValueError("La actividad debe ser uno de los valores de NivelActividad.")
        self._actividad = actividad

    @property
    def peso(self) -> float:
        """Getter para el atributo peso."""
        return self._peso

    @property
    def altura(self) -> float:
        """Getter para el atributo altura."""
        return self._altura

    @property
    def edad(self) -> int:
        """Getter para el atributo edad."""
        return self._edad

    @property
    def sexo(self) -> str:
        """Getter para el atributo sexo."""
        return self._sexo

    @property
    def actividad(self) -> NivelActividad:
        """Getter para el atributo actividad."""
        return self._actividad
 
    def calcular_requerimiento_calorico(self) -> float:
        if self._actividad == NivelActividad.SEDENTARIO:
            return  1.2
        elif self._actividad == NivelActividad.LEVEMENTE_ACTIVO:
            return  1.375
        elif self._actividad == NivelActividad.MODERADAMENTE_ACTIVO:
            return  1.55
        elif self._actividad == NivelActividad.MUY_ACTIVO:
            return  1.725
        elif self._actividad == NivelActividad.HIPERACTIVO:
            return  1.9
        else:
            raise ValueError("La actividad debe ser uno de los valores de NivelActividad.")
    