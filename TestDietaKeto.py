import unittest
from dietaKeto import DietaKeto
from alimento import Alimento
from categoriaAlimentos import CategoriaAlimentos
from nutriente import Nutriente

class TestDietaKeto(unittest.TestCase):
    def setUp(self):
        nutriente_manzana =  Nutriente(proteinas=100, carbohidratos=25, grasas=166.66666666666666, fibra=0, colesterol=0)
        nutriente_pollo = Nutriente(proteinas=30, carbohidratos=0, grasas=90, fibra=0, colesterol=85)
        nutriente_cebolla = Nutriente(proteinas=2, carbohidratos=5, grasas=1.1, fibra=1.5, colesterol=0)
        nutriente_pera = Nutriente(proteinas=1, carbohidratos=9, grasas=0.5, fibra=3, colesterol=0)

        self.manzana = Alimento(nombre="Manzana", nutrientes=nutriente_manzana, categoria=CategoriaAlimentos.FRUTAS)
        self.pollo = Alimento(nombre="Pollo", nutrientes=nutriente_pollo, categoria=CategoriaAlimentos.CARNES)
        self.cebolla = Alimento(nombre="Cebolla", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.VERDURAS)
        self.pera = Alimento(nombre="Pera", nutrientes=nutriente_pera, categoria=CategoriaAlimentos.FRUTAS)

        self.dieta_keto = DietaKeto(nombre="Dieta Cetogénica")

    def test_init(self):
        self.assertEqual(self.dieta_keto.nombre, "Dieta Cetogénica")

    def test_agregar_menu(self):
        self.dieta_keto.agregar_menu(0, [self.manzana])
        self.assertEqual(len(self.dieta_keto.semana[0]), 1)

    def test_es_dieta_correcta_cetogenica(self):
        self.dieta_keto.agregar_menu(0, [self.manzana]) 
        self.assertTrue(self.dieta_keto.es_dieta_correcta())

    def test_es_dieta_correcta_no_cetogenica(self):
        self.dieta_keto.agregar_menu(0, [ self.cebolla, self.pera])
        self.assertFalse(self.dieta_keto.es_dieta_correcta())
  
if __name__ == "__main__":
    unittest.main()
