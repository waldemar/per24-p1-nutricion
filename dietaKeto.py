from dieta import Dieta
from alimento import Alimento
from categoriaAlimentos import CategoriaAlimentos
from nutriente import Nutriente
from typing import List, Dict
from persona import Persona
from persona import NivelActividad

class DietaKeto(Dieta):
    
    def __init__(self, nombre: str) -> None:
        super().__init__(nombre)  
       
        
    def es_dieta_correcta(self) -> bool:
        super().calcular_contenido_nutricional()
        contenido_nutricional = self.contenido_nutricional
        print("Contenido nutriciodddnal:", contenido_nutricional) 

        if contenido_nutricional is None:
            return False
        
        print("Contenido nutricional:", contenido_nutricional)
        calorias = contenido_nutricional.get('calorias', 0)
        proteina = contenido_nutricional.get('proteinas', 0)
        carbohidratos = contenido_nutricional.get('carbohidratos', 0)
        grasas = contenido_nutricional.get('grasas', 0)
        
        print(calorias)
        porcentaje_grasas = ((grasas *9)/ calorias ) * 100
        print(porcentaje_grasas)
        porcentaje_proteinas = ((proteina *4)/ calorias ) * 100
        porcentaje_carbohidratos = ((carbohidratos *4)/ calorias) * 100
        print(porcentaje_proteinas,porcentaje_carbohidratos,porcentaje_grasas)
        if porcentaje_grasas >= 75 and porcentaje_proteinas <= 20 and porcentaje_carbohidratos <= 5:
            return True
        else:
            return False

nutriente_manzana = Nutriente(proteinas=100, carbohidratos=25, grasas=166.66666666666666, fibra=0, colesterol=0)

manzana = Alimento(nombre="Manzana", nutrientes=nutriente_manzana, categoria=CategoriaAlimentos.FRUTAS)

dieta_keto = DietaKeto(nombre="Dieta Cetogénica")
dieta_keto.agregar_menu(0, [manzana])  


print("¿Es la dieta cetogénica?", dieta_keto.es_dieta_correcta())