from enum import Enum

class CategoriaAlimentos(Enum):
    """
    Enumeración para representar diferentes categorías de alimentos.
    """

    FRUTAS = "Frutas"
    VERDURAS = "Verduras"
    CARNES = "Carnes"
    LACTEOS = "Lácteos"
    CEREALES = "Cereales"
    LEGUMBRES = "Legumbres"
    PESCADOS = "Pescados"
    FRUTOS_SECOS = "Frutos secos"
    POSTRES = "Postres"
