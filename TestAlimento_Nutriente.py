import unittest
from categoriaAlimentos import CategoriaAlimentos
from nutriente import Nutriente
from alimento import Alimento

class TestAlimento(unittest.TestCase):
    def setUp(self):
        self.nutrientes = Nutriente(10, 20, 30, 5, 0)
        self.categoria = CategoriaAlimentos.FRUTAS
        self.alimento = Alimento("Manzana", self.nutrientes, self.categoria)

    def test_init(self):
        self.assertEqual(self.alimento.nombre, "Manzana")
        self.assertEqual(self.alimento.nutrientes, self.nutrientes)
        self.assertEqual(self.alimento.categoria, self.categoria)

    def test_nombre(self):
        self.alimento.nombre = "Pera"
        self.assertEqual(self.alimento.nombre, "Pera")

    def test_nutrientes(self):
        new_nutrientes = Nutriente(15, 25, 35, 8, 2)
        self.alimento.nutrientes = new_nutrientes
        self.assertEqual(self.alimento.nutrientes, new_nutrientes)

    def test_categoria(self):
        new_categoria = CategoriaAlimentos.CARNES
        self.alimento.categoria = new_categoria
        self.assertEqual(self.alimento.categoria, new_categoria)

class TestNutriente(unittest.TestCase):
    def setUp(self):
        self.nutriente = Nutriente(10, 20, 30, 5, 0)

    def test_init(self):
        self.assertEqual(self.nutriente.proteinas, 10)
        self.assertEqual(self.nutriente.carbohidratos, 20)
        self.assertEqual(self.nutriente.grasas, 30)
        self.assertEqual(self.nutriente.fibra, 5)
        self.assertEqual(self.nutriente.colesterol, 0)

    def test_proteinas(self):
        self.nutriente.proteinas = 15
        self.assertEqual(self.nutriente.proteinas, 15)

    def test_carbohidratos(self):
        self.nutriente.carbohidratos = 25
        self.assertEqual(self.nutriente.carbohidratos, 25)

    def test_grasas(self):
        self.nutriente.grasas = 35
        self.assertEqual(self.nutriente.grasas, 35)

    def test_fibra(self):
        self.nutriente.fibra = 8
        self.assertEqual(self.nutriente.fibra, 8)

    def test_colesterol(self):
        self.nutriente.colesterol = 5
        self.assertEqual(self.nutriente.colesterol, 5)

    def test_calcular_calorias(self):
        calorias_esperadas = 4 * 10 + 4 * 20 + 9 * 30
        self.assertEqual(self.nutriente.calcular_calorias(), calorias_esperadas)

if __name__ == '__main__':
    unittest.main()
