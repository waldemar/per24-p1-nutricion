from dieta import Dieta
from alimento import Alimento
from categoriaAlimentos import CategoriaAlimentos
from nutriente import Nutriente
from typing import List, Dict


class dietaVegetariana(Dieta):
    
    def __init__(self, nombre: str, suplementos: List[str]) -> None:
        super().__init__(nombre)  
        self.semana: List[List[Alimento]] = [[] for _ in range(7)]
        self.suplementos: List[str] = suplementos
    def comprobar_alimento_animal(self):
        for dia in self.semana:
           return  bool([alimento for alimento in dia if alimento.categoria == CategoriaAlimentos.CARNES or alimento.categoria == CategoriaAlimentos.LACTEOS or alimento.categoria == CategoriaAlimentos.PESCADOS])
            
    
    def agregar_suplemento(self, suplemento: str) -> None:
        self.suplementos.append(suplemento)

    def eliminar_suplemento(self, suplemento: str) -> None:
        if suplemento in self.suplementos:
            self.suplementos.remove(suplemento)
        else:
            raise ValueError(f"El suplemento '{suplemento}' no está en la lista de suplementos")


nutriente_manzana = Nutriente(proteinas=0.3, carbohidratos=25, grasas=0.4, fibra=4.4, colesterol=0)
nutriente_pollo = Nutriente(proteinas=31, carbohidratos=20000.2, grasas=4.8, fibra=0, colesterol=85)
nutriente_cebolla = Nutriente(proteinas=321, carbohidratos=2.2, grasas=2.8, fibra=1, colesterol=85)
nutriente_pera = Nutriente(proteinas=3221, carbohidratos=22.2, grasas=2.8, fibra=1, colesterol=2)

manzana = Alimento(nombre="Manzana", nutrientes=nutriente_manzana, categoria=CategoriaAlimentos.FRUTAS)
pollo = Alimento(nombre="Pollo", nutrientes=nutriente_pollo, categoria=CategoriaAlimentos.CARNES)
cebolla = Alimento(nombre="Cebolla", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.VERDURAS)
pera = Alimento(nombre="Pera", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.FRUTAS)

suplementos = ["Vitamina B12", "Omega-3"]
dieta_semana = dietaVegetariana("Dieta Semanal", suplementos)
dieta_semana = dietaVegetariana(nombre="Dieta Semanal", suplementos= suplementos)
dieta_semana.agregar_menu(0, [manzana, pollo])
dieta_semana.agregar_menu(1, [pera, cebolla])

print(dieta_semana.comprobar_alimento_animal())

