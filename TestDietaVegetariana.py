import unittest
from dietaVegetariana import dietaVegetariana
from alimento import Alimento
from categoriaAlimentos import CategoriaAlimentos
from nutriente import Nutriente


class TestDietaVegetariana(unittest.TestCase):
    def setUp(self):
        nutriente_manzana = Nutriente(proteinas=0.3, carbohidratos=25, grasas=0.4, fibra=4.4, colesterol=0)
        nutriente_pollo = Nutriente(proteinas=31, carbohidratos=20000.2, grasas=4.8, fibra=0, colesterol=85)
        nutriente_cebolla = Nutriente(proteinas=321, carbohidratos=2.2, grasas=2.8, fibra=1, colesterol=85)
        nutriente_pera = Nutriente(proteinas=3221, carbohidratos=22.2, grasas=2.8, fibra=1, colesterol=2)

        self.manzana = Alimento(nombre="Manzana", nutrientes=nutriente_manzana, categoria=CategoriaAlimentos.FRUTAS)
        self.pollo = Alimento(nombre="Pollo", nutrientes=nutriente_pollo, categoria=CategoriaAlimentos.CARNES)
        self.cebolla = Alimento(nombre="Cebolla", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.VERDURAS)
        self.pera = Alimento(nombre="Pera", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.FRUTAS)

        self.suplementos = ["Vitamina B12", "Omega-3", "Hierro", "Calcio"]

        self.dieta_semana = dietaVegetariana("Dieta Semanal", self.suplementos)

    def test_init(self):
        self.assertEqual(self.dieta_semana.nombre, "Dieta Semanal")
        self.assertEqual(self.dieta_semana.suplementos, self.suplementos)

    def test_agregar_menu(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.dieta_semana.agregar_menu(1, [self.pera, self.cebolla])
        self.assertEqual(len(self.dieta_semana.semana[0]), 2)
        self.assertEqual(len(self.dieta_semana.semana[1]), 2)

    def test_comprobar_no_alimento_animal_con_animal(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.assertTrue(self.dieta_semana.comprobar_alimento_animal())

    def test_comprobar_no_alimento_animal_sin_animal(self):
        self.dieta_semana.agregar_menu(0, [self.pera, self.cebolla])
        self.assertFalse(self.dieta_semana.comprobar_alimento_animal())

    def test_agregar_suplemento(self):
        self.dieta_semana.agregar_suplemento("Calcio")
        self.assertIn("Calcio", self.dieta_semana.suplementos)

    def test_eliminar_suplemento_existente(self):
        self.dieta_semana.eliminar_suplemento("Hierro")
        self.assertNotIn("Hierro", self.dieta_semana.suplementos)

    def test_eliminar_suplemento_inexistente(self):
        with self.assertRaises(ValueError):
            self.dieta_semana.eliminar_suplemento("Zinc")

if __name__ == "__main__":
    unittest.main()

