from categoriaAlimentos import CategoriaAlimentos
from nutriente import Nutriente
class Alimento:
    """Clase que representa un Alimento."""

    def __init__(self, nombre: str, nutrientes: Nutriente, categoria: CategoriaAlimentos) -> None:
        """
        Inicializa un nuevo objeto Alimento.

        :param str nombre: Nombre del alimento.
        :param object nutrientes: Objeto que representa los nutrientes del alimento.
        :param CategoriaAlimento categoria: Categoría del alimento (enum).
        """
        self._nombre = nombre
        self._nutrientes = nutrientes
        self._categoria = categoria

    @property
    def nombre(self) -> str:
        """Getter para el nombre del alimento."""
        return self._nombre

    @nombre.setter
    def nombre(self, value: str) -> None:
        """Setter para el nombre del alimento."""
        self._nombre = value

    @property
    def nutrientes(self) -> object:
        """Getter para los nutrientes del alimento."""
        return self._nutrientes

    @nutrientes.setter
    def nutrientes(self, value: object) -> None:
        """Setter para los nutrientes del alimento."""
        self._nutrientes = value

    @property
    def categoria(self) -> CategoriaAlimentos:
        """Getter para la categoría del alimento."""
        return self._categoria

    @categoria.setter
    def categoria(self, value: CategoriaAlimentos) -> None:
        """Setter para la categoría del alimento."""
        self._categoria = value
