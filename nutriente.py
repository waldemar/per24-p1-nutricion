class Nutriente:
    """Clase que representa un nutriente."""

    total_nutrientes = 0  

    def __init__(self, proteinas: float, carbohidratos: float, grasas: float, fibra: float, colesterol: float) -> None:
        """
        Constructor de la clase Nutriente.
        Clase para representar un nutriente con sus respectivos valores.

        :param float proteinas: Valor de proteínas en gramos.
        :param float carbohidratos: Valor de carbohidratos en gramos.
        :param float grasas: Valor de grasas en gramos.
        :param float fibra: Valor de fibra en gramos.
        :param float colesterol: Valor de colesterol en miligramos.

         Atributos:
        - proteinas (float): Valor de proteínas en gramos.
        - carbohidratos (float): Valor de carbohidratos en gramos.
        - grasas (float): Valor de grasas en gramos.
        - fibra (float): Valor de fibra en gramos.
        - colesterol (float): Valor de colesterol en miligramos.

         Métodos:
        - set_proteinas(valor): Establece el valor de proteínas.
        - set_carbohidratos(valor): Establece el valor de carbohidratos.
        - set_grasas(valor): Establece el valor de grasas.
        - set_fibra(valor): Establece el valor de fibra.
        - set_colesterol(valor): Establece el valor de colesterol.
        - calcular_calorias(): Calcula las calorías totales basadas en los valores de proteínas, carbohidratos y grasas.
        """
        self._proteinas: float = proteinas
        self._carbohidratos: float = carbohidratos
        self._grasas: float = grasas
        self._fibra: float = fibra
        self._colesterol: float = colesterol
        Nutriente.total_nutrientes += 1 

    @property
    def proteinas(self) -> float:
        """
        Obtiene el valor de proteínas.

        :return: Valor de proteínas en gramos.
        :rtype: float
        """
        return self._proteinas

    @proteinas.setter
    def proteinas(self, valor: float) -> None:
        """
        Establece el valor de proteínas.

        :param float valor: Nuevo valor de proteínas en gramos.
        """
        self._proteinas = valor

    @property
    def carbohidratos(self) -> float:
        """
        Obtiene el valor de carbohidratos.

        :return: Valor de carbohidratos en gramos.
        :rtype: float
        """
        return self._carbohidratos

    @carbohidratos.setter
    def carbohidratos(self, valor: float) -> None:
        """
        Establece el valor de carbohidratos.

        :param float valor: Nuevo valor de carbohidratos en gramos.
        """
        self._carbohidratos = valor

    @property
    def grasas(self) -> float:
        """
        Obtiene el valor de grasas.

        :return: Valor de grasas en gramos.
        :rtype: float
        """
        return self._grasas

    @grasas.setter
    def grasas(self, valor: float) -> None:
        """
        Establece el valor de grasas.

        :param float valor: Nuevo valor de grasas en gramos.
        """
        self._grasas = valor

    @property
    def fibra(self) -> float:
        """
        Obtiene el valor de fibra.

        :return: Valor de fibra en gramos.
        :rtype: float
        """
        return self._fibra

    @fibra.setter
    def fibra(self, valor: float) -> None:
        """
        Establece el valor de fibra.

        :param float valor: Nuevo valor de fibra en gramos.
        """
        self._fibra = valor

    @property
    def colesterol(self) -> float:
        """
        Obtiene el valor de colesterol.

        :return: Valor de colesterol en miligramos.
        :rtype: float
        """
        return self._colesterol

    @colesterol.setter
    def colesterol(self, valor: float) -> None:
        """
        Establece el valor de colesterol.

        :param float valor: Nuevo valor de colesterol en miligramos.
        """
        self._colesterol = valor

    def calcular_calorias(self) -> float:
        """
        Calcula las calorías totales basadas en los valores de proteínas, carbohidratos y grasas en gramos.
         el colesterol y la fibra no tienen calorías por su naturaleza biomolecular.
        :return: Calorías totales calculadas.
        :rtype: float
        """
       
        calorias = 4 * self.proteinas + 4 * self.carbohidratos + 9 * self.grasas
        return calorias

    @classmethod
    def obtener_total_nutrientes(cls):
        """
        Método de clase para obtener el número total de objetos nutrientes creados.
        """
        return cls.total_nutrientes
    




