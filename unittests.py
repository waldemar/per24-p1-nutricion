
import unittest
from alimento import Alimento
from categoriaAlimentos import CategoriaAlimentos
from nutriente import Nutriente
from persona import Persona, NivelActividad
from dieta import Dieta
from dietaKeto import DietaKeto
from dietaVegetariana import dietaVegetariana
from persona import Persona, NivelActividad

class TestAlimento(unittest.TestCase):
    def setUp(self):
        self.nutrientes = Nutriente(10, 20, 30, 5, 0)
        self.categoria = CategoriaAlimentos.FRUTAS
        self.alimento = Alimento("Manzana", self.nutrientes, self.categoria)

    def test_init(self):
        self.assertEqual(self.alimento.nombre, "Manzana")
        self.assertEqual(self.alimento.nutrientes, self.nutrientes)
        self.assertEqual(self.alimento.categoria, self.categoria)

    def test_nombre(self):
        self.alimento.nombre = "Pera"
        self.assertEqual(self.alimento.nombre, "Pera")

    def test_nutrientes(self):
        new_nutrientes = Nutriente(15, 25, 35, 8, 2)
        self.alimento.nutrientes = new_nutrientes
        self.assertEqual(self.alimento.nutrientes, new_nutrientes)

    def test_categoria(self):
        new_categoria = CategoriaAlimentos.CARNES
        self.alimento.categoria = new_categoria
        self.assertEqual(self.alimento.categoria, new_categoria)

class TestNutriente(unittest.TestCase):
    def setUp(self):
        self.nutriente = Nutriente(10, 20, 30, 5, 0)

    def test_init(self):
        self.assertEqual(self.nutriente.proteinas, 10)
        self.assertEqual(self.nutriente.carbohidratos, 20)
        self.assertEqual(self.nutriente.grasas, 30)
        self.assertEqual(self.nutriente.fibra, 5)
        self.assertEqual(self.nutriente.colesterol, 0)

    def test_proteinas(self):
        self.nutriente.proteinas = 15
        self.assertEqual(self.nutriente.proteinas, 15)

    def test_carbohidratos(self):
        self.nutriente.carbohidratos = 25
        self.assertEqual(self.nutriente.carbohidratos, 25)

    def test_grasas(self):
        self.nutriente.grasas = 35
        self.assertEqual(self.nutriente.grasas, 35)

    def test_fibra(self):
        self.nutriente.fibra = 8
        self.assertEqual(self.nutriente.fibra, 8)

    def test_colesterol(self):
        self.nutriente.colesterol = 5
        self.assertEqual(self.nutriente.colesterol, 5)

    def test_calcular_calorias(self):
        calorias_esperadas = 4 * 10 + 4 * 20 + 9 * 30
        self.assertEqual(self.nutriente.calcular_calorias(), calorias_esperadas)

class TestDieta(unittest.TestCase):
    def setUp(self):
        self.nutriente_manzana = Nutriente(proteinas=0.3, carbohidratos=25, grasas=0.4, fibra=4.4, colesterol=0)
        self.nutriente_pollo = Nutriente(proteinas=31, carbohidratos=20000.2, grasas=4.8, fibra=0, colesterol=85)
        self.nutriente_cebolla = Nutriente(proteinas=321, carbohidratos=2.2, grasas=2.8, fibra=1, colesterol=85)
        self.nutriente_pera = Nutriente(proteinas=3221, carbohidratos=22.2, grasas=2.8, fibra=1, colesterol=2)

        self.manzana = Alimento(nombre="Manzana", nutrientes=self.nutriente_manzana, categoria=CategoriaAlimentos.FRUTAS)
        self.pollo = Alimento(nombre="Pollo", nutrientes=self.nutriente_pollo, categoria=CategoriaAlimentos.CARNES)
        self.cebolla = Alimento(nombre="Cebolla", nutrientes=self.nutriente_cebolla, categoria=CategoriaAlimentos.VERDURAS)
        self.pera = Alimento(nombre="Pera", nutrientes=self.nutriente_pera, categoria=CategoriaAlimentos.FRUTAS)

        self.dieta_semana = Dieta(nombre="Dieta Semanal")

    def test_agregar_menu(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.assertEqual(len(self.dieta_semana.semana[0]), 2)

    def test_eliminar_alimento(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.dieta_semana.eliminar_alimento(0, 1)
        self.assertEqual(len(self.dieta_semana.semana[0]), 1)

    def test_calcular_contenido_nutricional(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.dieta_semana.agregar_menu(1, [self.pera, self.cebolla])
        self.dieta_semana.calcular_contenido_nutricional()
        self.assertNotEqual(self.dieta_semana.contenido_nutricional, {})
        self.assertIn('calorias', self.dieta_semana.contenido_nutricional)
        self.assertIn('proteinas', self.dieta_semana.contenido_nutricional)
        self.assertIn('carbohidratos', self.dieta_semana.contenido_nutricional)
        self.assertIn('grasas', self.dieta_semana.contenido_nutricional)

    def test_evaluar_dieta(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.dieta_semana.agregar_menu(1, [self.pera, self.cebolla])
        persona1 = Persona(peso=100, altura=1.80, edad=20, sexo='hombre', actividad=NivelActividad.HIPERACTIVO)
        self.dieta_semana.calcular_contenido_nutricional()
        resultado = self.dieta_semana.evaluar_dieta(persona1)
        self.assertIn(resultado, ["La dieta es adecuada", "La dieta es mala", "La dieta es buena"])

    def test_obtener_alimentos_dia(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        alimentos_dia_0 = self.dieta_semana.obtener_alimentos_dia(0)
        self.assertEqual(alimentos_dia_0, [self.manzana, self.pollo])

        alimentos_dia_1 = self.dieta_semana.obtener_alimentos_dia(1)
        self.assertEqual(alimentos_dia_1, [])

    def test_calcular_contenido_nutricional_dia(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.dieta_semana.agregar_menu(1, [self.pera, self.cebolla])
        contenido_nutricional_dia_0 = self.dieta_semana.calcular_contenido_nutricional_dia(0)
        contenido_nutricional_dia_1 = self.dieta_semana.calcular_contenido_nutricional_dia(1)

        self.assertEqual(contenido_nutricional_dia_0['calorias'], self.manzana.nutrientes.calcular_calorias() + self.pollo.nutrientes.calcular_calorias()) # type: ignore
        self.assertEqual(contenido_nutricional_dia_1['calorias'], self.pera.nutrientes.calcular_calorias() + self.cebolla.nutrientes.calcular_calorias()) # type: ignore

    def test_mostrar_contenido_nutricional(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.dieta_semana.agregar_menu(1, [self.pera, self.cebolla])
        self.dieta_semana.calcular_contenido_nutricional()

        contenido_nutricional_esperado = {
            'calorias': self.manzana.nutrientes.calcular_calorias() + self.pollo.nutrientes.calcular_calorias() + self.pera.nutrientes.calcular_calorias() + self.cebolla.nutrientes.calcular_calorias(), # type: ignore
            'proteinas': self.manzana.nutrientes.proteinas + self.pollo.nutrientes.proteinas + self.pera.nutrientes.proteinas + self.cebolla.nutrientes.proteinas, # type: ignore
            'carbohidratos': self.manzana.nutrientes.carbohidratos + self.pollo.nutrientes.carbohidratos + self.pera.nutrientes.carbohidratos + self.cebolla.nutrientes.carbohidratos, # type: ignore
            'grasas': self.manzana.nutrientes.grasas + self.pollo.nutrientes.grasas + self.pera.nutrientes.grasas + self.cebolla.nutrientes.grasas # type: ignore
        }

        self.assertEqual(self.dieta_semana.contenido_nutricional, contenido_nutricional_esperado)

class TestDietaKeto(unittest.TestCase):
     def setUp(self):
        nutriente_manzana =  Nutriente(proteinas=100, carbohidratos=25, grasas=166.66666666666666, fibra=0, colesterol=0)
        nutriente_pollo = Nutriente(proteinas=30, carbohidratos=0, grasas=90, fibra=0, colesterol=85)
        nutriente_cebolla = Nutriente(proteinas=2, carbohidratos=5, grasas=1.1, fibra=1.5, colesterol=0)
        nutriente_pera = Nutriente(proteinas=1, carbohidratos=9, grasas=0.5, fibra=3, colesterol=0)

        self.manzana = Alimento(nombre="Manzana", nutrientes=nutriente_manzana, categoria=CategoriaAlimentos.FRUTAS)
        self.pollo = Alimento(nombre="Pollo", nutrientes=nutriente_pollo, categoria=CategoriaAlimentos.CARNES)
        self.cebolla = Alimento(nombre="Cebolla", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.VERDURAS)
        self.pera = Alimento(nombre="Pera", nutrientes=nutriente_pera, categoria=CategoriaAlimentos.FRUTAS)

        self.dieta_keto = DietaKeto(nombre="Dieta Cetogénica")

        def test_init(self):
            self.assertEqual(self.dieta_keto.nombre, "Dieta Cetogénica")

        def test_agregar_menu(self):
            self.dieta_keto.agregar_menu(0, [self.manzana])
            self.assertEqual(len(self.dieta_keto.semana[0]), 1)

        def test_es_dieta_correcta_cetogenica(self):
            self.dieta_keto.agregar_menu(0, [self.manzana]) 
            self.assertTrue(self.dieta_keto.es_dieta_correcta())

        def test_es_dieta_correcta_no_cetogenica(self):
            self.dieta_keto.agregar_menu(0, [ self.cebolla, self.pera])
            self.assertFalse(self.dieta_keto.es_dieta_correcta())

class TestDietaVegetariana(unittest.TestCase):
    def setUp(self):
        nutriente_manzana = Nutriente(proteinas=0.3, carbohidratos=25, grasas=0.4, fibra=4.4, colesterol=0)
        nutriente_pollo = Nutriente(proteinas=31, carbohidratos=20000.2, grasas=4.8, fibra=0, colesterol=85)
        nutriente_cebolla = Nutriente(proteinas=321, carbohidratos=2.2, grasas=2.8, fibra=1, colesterol=85)
        nutriente_pera = Nutriente(proteinas=3221, carbohidratos=22.2, grasas=2.8, fibra=1, colesterol=2)

        self.manzana = Alimento(nombre="Manzana", nutrientes=nutriente_manzana, categoria=CategoriaAlimentos.FRUTAS)
        self.pollo = Alimento(nombre="Pollo", nutrientes=nutriente_pollo, categoria=CategoriaAlimentos.CARNES)
        self.cebolla = Alimento(nombre="Cebolla", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.VERDURAS)
        self.pera = Alimento(nombre="Pera", nutrientes=nutriente_cebolla, categoria=CategoriaAlimentos.FRUTAS)

        self.suplementos = ["Vitamina B12", "Omega-3","Hierro", "Calcio"]

        self.dieta_semana = dietaVegetariana("Dieta Semanal", self.suplementos)

    def test_init(self):
        self.assertEqual(self.dieta_semana.nombre, "Dieta Semanal")
        self.assertEqual(self.dieta_semana.suplementos, self.suplementos)

    def test_agregar_menu(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.dieta_semana.agregar_menu(1, [self.pera, self.cebolla])
        self.assertEqual(len(self.dieta_semana.semana[0]), 2)
        self.assertEqual(len(self.dieta_semana.semana[1]), 2)

    def test_comprobar_no_alimento_animal_con_animal(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.pollo])
        self.assertTrue(self.dieta_semana.comprobar_alimento_animal)

    def test_comprobar_no_alimento_animal_sin_animal(self):
        self.dieta_semana.agregar_menu(0, [self.manzana, self.cebolla])
        self.assertFalse(self.dieta_semana.comprobar_alimento_animal())

    def test_agregar_suplemento(self):
        self.dieta_semana.agregar_suplemento("Calcio")
        self.assertIn("Calcio", self.dieta_semana.suplementos)

    def test_eliminar_suplemento_existente(self):
        self.dieta_semana.eliminar_suplemento("Hierro")
        self.assertNotIn("Hierro", self.dieta_semana.suplementos)

    def test_eliminar_suplemento_inexistente(self):
        with self.assertRaises(ValueError):
            self.dieta_semana.eliminar_suplemento("Zinc")
class TestPersona(unittest.TestCase):
    def setUp(self):
        self.persona = Persona(peso=70, altura=175, edad=30, sexo='hombre', actividad=NivelActividad.MODERADAMENTE_ACTIVO)

    def test_init(self):
        self.assertEqual(self.persona.peso, 70)
        self.assertEqual(self.persona.altura, 175)
        self.assertEqual(self.persona.edad, 30)
        self.assertEqual(self.persona.sexo, 'hombre')
        self.assertEqual(self.persona.actividad, NivelActividad.MODERADAMENTE_ACTIVO)

    def test_calcular_requerimiento_calorico_sedentario(self):
        persona_sedentaria = Persona(peso=70, altura=175, edad=30, sexo='hombre', actividad=NivelActividad.SEDENTARIO)
        self.assertAlmostEqual(persona_sedentaria.calcular_requerimiento_calorico(), 1.2, places=2)

    def test_calcular_requerimiento_calorico_levemente_activo(self):
        persona_levemente_activa = Persona(peso=70, altura=175, edad=30, sexo='hombre', actividad=NivelActividad.LEVEMENTE_ACTIVO)
        self.assertAlmostEqual(persona_levemente_activa.calcular_requerimiento_calorico(), 1.375, places=2)

    def test_calcular_requerimiento_calorico_moderadamente_activo(self):
        self.assertAlmostEqual(self.persona.calcular_requerimiento_calorico(), 1.55, places=2)

    def test_calcular_requerimiento_calorico_muy_activo(self):
        persona_muy_activa = Persona(peso=70, altura=175, edad=30, sexo='hombre', actividad=NivelActividad.MUY_ACTIVO)
        self.assertAlmostEqual(persona_muy_activa.calcular_requerimiento_calorico(), 1.725, places=2)

    def test_calcular_requerimiento_calorico_hiperactivo(self):
        persona_hiperactiva = Persona(peso=70, altura=175, edad=30, sexo='hombre', actividad=NivelActividad.HIPERACTIVO)
        self.assertAlmostEqual(persona_hiperactiva.calcular_requerimiento_calorico(), 1.9, places=2)
        #compara con una relación de dos decimales
    def test_calcular_requerimiento_calorico_invalido(self):
        with self.assertRaises(ValueError):
            persona_invalida = Persona(peso=70, altura=175, edad=30, sexo='hombre', actividad=NivelActividad(9))
            #creo una instancia de NiveldeActividad para que pase la excepción, si no no la pasaría porque saltaría una exepción antes de no pasar una categoría correcta
if __name__ == '__main__':
    unittest.main()
